package com.gosugamer;

import android.app.Application;
import android.content.Context;

/**
 * Created by n004635 on 2014/7/23.
 */
public class App extends Application {
    private static Context mContext;

    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
    }

    public static Context getContext() {
        return mContext;
    }
}
