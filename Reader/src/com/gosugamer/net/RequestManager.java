package com.gosugamer.net;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;

import com.gosugamer.App;


/**
 * Created by n004635 on 2014/7/23.
 */
public class RequestManager {
    //http://infinigag-us.aws.af.cm/hot/0
    public static RequestQueue mRequstQueue = Volley.newRequestQueue(App.getContext());

    private RequestManager() {
    }

    public static void addRequest(Request<?> request, Object tag) {
        if (tag != null) {
            request.setTag(tag);
        }
        mRequstQueue.add(request);
    }

    public static void cancelAll(Object tag) {
        mRequstQueue.cancelAll(tag);
    }
}
