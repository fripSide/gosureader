package com.base.paser;

/**
 * Created by n004635 on 2014/7/16.
 */
public abstract class BaseParser {
    public abstract String getWebContent(String uri) throws Exception;
}
