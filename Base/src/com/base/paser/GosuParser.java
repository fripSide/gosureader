package com.base.paser;

import com.base.utils.Constants;
import org.apache.http.client.fluent.Request;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Created by n004635 on 2014/7/16.
 *
 */
public class GosuParser extends BaseParser {

//    public String getWebContent() throws Exception {
//        String content = "";
//        System.out.println("Start download html...");
//        CloseableHttpClient httpClient = HttpClients.createDefault();
//        try {
//            HttpGet httpGet = new HttpGet(Constants.GUSUINDEX);
//            CloseableHttpResponse res = httpClient.execute(httpGet);
//            try {
//                System.out.println(res.getStatusLine());
//                HttpEntity entity = res.getEntity();
//                content = EntityUtils.toString(entity);
//                EntityUtils.consume(entity);
//            } finally {
//                res.close();
//            }
//        } finally {
//            httpClient.close();
//        }
//        System.out.println("End download html...");
//        return content;
//    }

    public String getWebContent(String uri) throws Exception {
        return Request.Get(uri).execute().returnContent().asString();
    }

    public void IndexParser(String content) {

    }

    public String testJsoup(String uri) {
        StringBuilder sb = new StringBuilder();
        try {
            Document doc = Jsoup.connect(uri).userAgent("Mozilla").get();
//            Elements links = doc.select("a[href]");
//            for (Element link : links) {
//                sb.append(link.text());
//                System.out.println(link.attr("href"));
//                System.out.println(link.attr("abs:href"));
//            }
//            System.out.printf("\nLinks: (%d)", links.size());
            Elements articles = doc.select("div#showcase-content-3");
            for (Element article : articles) {
//                System.out.println(article.text());
                System.out.println(article.toString());
            }
            Elements details = doc.select("div.latest-articles");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        System.out.println("paser start...");
        GosuParser gosu = new GosuParser();
        try {
            String str = gosu.testJsoup(Constants.GUSUINDEX);
            System.out.println(str);
//            String c = gosu.getWebContent(Constants.GUSUINDEX);
////            System.out.println(c);
//            FileWriter fw = new FileWriter("index.html");
//            fw.write(c);
//            fw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
