package com.base.utils;

/**
 * Created by n004635 on 2014/7/16.
 * Constants URLs
 */
public class Constants {

    public static final String GUSUINDEX = "http://www.gosugamers.net/dota2";
    public static final String GUSURANKING = "http://www.gosugamers.net/dota2/rankings#team";
}
